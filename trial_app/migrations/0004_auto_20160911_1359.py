# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trial_app', '0003_auto_20160911_1337'),
    ]

    operations = [
        migrations.RenameField(
            model_name='orders',
            old_name='created_on',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='orders',
            old_name='updated_on',
            new_name='updated_at',
        ),
    ]
