# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trial_app', '0002_store_domain'),
    ]

    operations = [
        migrations.RenameField(
            model_name='orders',
            old_name='customer_id',
            new_name='customer',
        ),
        migrations.RenameField(
            model_name='orders',
            old_name='store_id',
            new_name='store',
        ),
    ]
