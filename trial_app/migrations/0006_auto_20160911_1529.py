# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trial_app', '0005_auto_20160911_1407'),
    ]

    operations = [
        migrations.RenameField(
            model_name='orders',
            old_name='amount',
            new_name='total_price',
        ),
        migrations.AddField(
            model_name='orders',
            name='billing_address',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orders',
            name='currency',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orders',
            name='financial_status',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orders',
            name='shipping_address',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
    ]
