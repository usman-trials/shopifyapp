# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trial_app', '0007_auto_20160912_0937'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='shopify_customer_id',
            field=models.IntegerField(default=b'0', unique=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orders',
            name='shopify_order_id',
            field=models.IntegerField(default=b'0', unique=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='store',
            name='shopify_store_id',
            field=models.IntegerField(default=b'0', unique=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='customer',
            name='id',
            field=models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
        ),
        migrations.AlterField(
            model_name='orders',
            name='id',
            field=models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
        ),
        migrations.AlterField(
            model_name='store',
            name='id',
            field=models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
        ),
    ]
