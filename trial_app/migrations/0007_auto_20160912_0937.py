# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trial_app', '0006_auto_20160911_1529'),
    ]

    operations = [
        migrations.AddField(
            model_name='orders',
            name='incorrect_address',
            field=models.IntegerField(default=b'0'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orders',
            name='notification_sent',
            field=models.IntegerField(default=b'0'),
            preserve_default=True,
        ),
    ]
