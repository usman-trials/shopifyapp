# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('first_name', models.TextField()),
                ('last_name', models.TextField()),
                ('email', models.CharField(max_length=80)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Orders',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.TextField()),
                ('order_number', models.IntegerField()),
                ('amount', models.TextField()),
                ('email', models.CharField(max_length=80)),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
                ('customer_id', models.ForeignKey(to='trial_app.Customer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='orders',
            name='store_id',
            field=models.ForeignKey(to='trial_app.Store'),
            preserve_default=True,
        ),
    ]
