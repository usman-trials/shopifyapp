# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trial_app', '0004_auto_20160911_1359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orders',
            name='created_at',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='orders',
            name='updated_at',
            field=models.DateTimeField(),
        ),
    ]
