import shopify, sys, json
from django.shortcuts import render_to_response
from django.template import RequestContext
from shopify_app.decorators import shop_login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import View, RedirectView
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from helper import Helper, Struct


# datetime.datetime.now().isoformat()
class Home(View):
    template_name = 'trial_app/home.html'
    helper = Helper()

    @method_decorator(shop_login_required)
    def get(self, request, *args, **kwargs):
        context_instance = RequestContext(request)
        self.helper.add_store(context_instance['current_shop'])
        shopify_store = self.helper.get_shopify_store(context_instance)
        last_order = self.helper.get_last_order(shopify_store.id)

        if last_order:
            recent_orders = shopify.Order.find(created_at_min=last_order, order="created_at DESC")
        else:
            recent_orders = shopify.Order.find(order="created_at DESC")

        self.helper.add_orders(recent_orders, shopify_store)
        return render_to_response(self.template_name, {
            'all_orders': {
                'Recent Orders': recent_orders,
                'Incorrect Address Orders': self.helper.get_incorrect_address_orders(shopify_store.id),
                'All Orders': self.helper.get_orders(shopify_store.id),
            }
        }, context_instance=context_instance)


class Webhooks(View):
    helper = Helper()

    def post(self, request, *args, **kwargs):
        try:
            domain = request.META.get('HTTP_X_SHOPIFY_SHOP_DOMAIN')
            hmac_hash = request.META['HTTP_X_SHOPIFY_HMAC_SHA256']
            data = json.loads(request.body)

            if not self.helper.hmac_verification(request.body, hmac_hash):
                return HttpResponseForbidden()

            order = Struct(**data)
            store = self.helper.get_shopify_store(domain=domain, context='')
            order.shipping_address = Struct(**order.shipping_address)
            order.billing_address = Struct(**order.billing_address)
            order.customer = Struct(**order.customer)
            self.helper.add_order(store=store, order=order)
        except Exception as e:
            return HttpResponseBadRequest()

        return HttpResponse('')
