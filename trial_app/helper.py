from models import Store, Orders, Customer
from django.core.mail import EmailMessage, send_mail
import datetime, time, os, hashlib, base64, hmac, re, logging


class Helper(object):
    Log = 1
    API_SECRET = 'ade1b72ce854ca3fa72446b8c14953d38ef4475c716136148629354240d786ca'
    logger = logging.getLogger(__name__)

    def get_shopify_store(self, context, domain=''):
        return Store.objects.filter(domain=domain)[0] if domain else \
            Store.objects.filter(shopify_store_id=context['current_shop'].id)[0]

    def add_store(self, shopify_store):
        store = Store.objects.filter(shopify_store_id=shopify_store.id)
        if not store:
            Store(shopify_store_id=shopify_store.id, name=shopify_store.name, domain=shopify_store.domain).save()

    def get_last_order(self, store_id):
        try:
            order = Orders.objects.filter(store_id=store_id).order_by('-created_at')
            return order[0].created_at if order else ''
        except Exception as e:
            self.log_response(e, 'get_last_order', 'exception')
            return ''

    def get_orders(self, store_id, order_id=''):
        try:
            return Orders.objects.filter(shopify_order_id=order_id, store_id=store_id)[
                0] if order_id else Orders.objects.filter(
                    store_id=store_id)
        except Exception as e:
            self.log_response(e, 'get_orders', 'exception')
            return ''

    def get_incorrect_address_orders(self, store_id, order_id=''):
        try:
            return Orders.objects.filter(shopify_order_id=order_id, store_id=store_id,
                                         incorrect_address=1) if order_id else Orders.objects.filter(
                    store_id=store_id, incorrect_address=1)
        except Exception as e:
            self.log_response(e, 'get_incorrect_address_orders', 'exception')
            return ''

    def add_orders(self, recent_orders, store):
        try:
            orders = []
            for order in recent_orders:
                data = self.send_notification(store, order)
                notification_sent = data['notification_sent']
                incorrect_address = data['incorrect_address']

                if not self.get_orders(store.id, order.id):
                    customer = self.get_customer(order)
                    customer = customer if customer else self.add_customer(order)
                    orders.append(
                            Orders(shopify_order_id=order.id, store=store, name=order.name,
                                   order_number=order.order_number,
                                   customer=customer, total_price=order.total_price, currency=order.currency,
                                   financial_status=order.financial_status, email=order.email,
                                   created_at=order.created_at, updated_at=order.updated_at,
                                   shipping_address=order.shipping_address.address1,
                                   billing_address=order.billing_address.address1, notification_sent=notification_sent,
                                   incorrect_address=incorrect_address))

            if orders:
                Orders.objects.bulk_create(orders)
        except Exception as e:
            self.log_response(e, 'add_orders', 'exception')

    def add_order(self, order, store):
        try:
            data = self.send_notification(store, order)
            notification_sent = data['notification_sent']
            incorrect_address = data['incorrect_address']

            if not self.get_orders(store.id, order.id):
                customer = self.get_customer(order)
                customer = customer if customer else self.add_customer(order)
                orders = Orders(shopify_order_id=order.id, store=store, name=order.name,
                                order_number=order.order_number,
                                customer=customer, total_price=order.total_price, currency=order.currency,
                                financial_status=order.financial_status, email=order.email,
                                created_at=order.created_at, updated_at=order.updated_at,
                                shipping_address=order.shipping_address.address1,
                                billing_address=order.billing_address.address1, notification_sent=notification_sent,
                                incorrect_address=incorrect_address)
                orders.save()

        except Exception as e:
            self.log_response(e, 'add_order', 'exception')

    def get_customer(self, order):
        try:
            customer = Customer.objects.filter(shopify_customer_id=order.customer.id)
            return customer[0] if customer else ''
        except Exception as e:
            self.log_response(e, 'get_customer', 'exception')
            return ''

    def add_customer(self, order):
        try:
            customer = Customer(shopify_customer_id=order.customer.id, first_name=order.customer.first_name,
                                last_name=order.customer.last_name, email=order.customer.email)

            customer.save()
            return customer
        except Exception as e:
            self.log_response(e, 'add_customer', 'exception')
            return ''

    def send_notification(self, store, order):
        try:
            response = {
                "incorrect_address": 0,
                "notification_sent": 0,
            }
            order_details = self.get_orders(store.id, order.id)
            incorrect_shipping_address = self.contains_non_english_chars(order.shipping_address.address1, order)
            incorrect_billing_address = self.contains_non_english_chars(order.billing_address.address1, order)
            response['incorrect_address'] = 1 if (incorrect_shipping_address or incorrect_billing_address) else 0
            if (not order_details or order_details.notification_sent != 1) and (incorrect_shipping_address
                                                                                or incorrect_billing_address):
                response['notification_sent'] = 1
                self.send_email(store=store, order=order, shipping_address=incorrect_shipping_address,
                                billing_address=incorrect_billing_address)
            elif order_details and (not incorrect_shipping_address and not incorrect_billing_address):
                response['incorrect_address'] = 0
                Orders.objects.filter(shopify_order_id=order.id).update(incorrect_address=0)
            else:
                response['incorrect_address'] = 1
                Orders.objects.filter(shopify_order_id=order.id).update(incorrect_address=1)
        except Exception as e:
            self.log_response(e, 'send_notification', 'exception')

        return response

    def contains_non_english_chars(self, string, order):
        try:
            self.log_response(string, 'contains_non_english_chars', 'message')
            string.decode('ascii')
            if not re.match("^[A-Za-z0-9_-]*$", string):
                raise Exception('Incorrect Address')
        except (UnicodeDecodeError, Exception) as e:
            self.log_response(e, 'contains_non_english_chars', 'exception')
            Orders.objects.filter(shopify_order_id=order.id).update(incorrect_address=1)
            return True
        else:
            return False

    def send_email(self, store, order, shipping_address, billing_address):
        try:
            link = ('https://%s/admin/orders/%s' % (store.domain, order.id))
            address = 'billing and shipping addresses' if shipping_address and billing_address else (
                'shipping address' if shipping_address else 'billing address')
            subject = 'Incorrect Address'
            html_message = """
                        Dear %s,<br><br>
                        You have entered incorrect %s for Order # %s. <br>
                        Please review the following link.
                        <a href='%s'> %s </a><br><br>
                        Regards,<br>%s
                      """ % (order.email, address, order.order_number, link, link, store.name)
            text_message = html_message.replace('<br>', '\n')
            send_mail(subject=subject, message=text_message, html_message=html_message, from_email=store.name,
                      recipient_list=[order.email])
            self.log_response('Mail sent!!!', 'send_email', 'message')
            Orders.objects.filter(shopify_order_id=order.id).update(notification_sent=1)
        except Exception as e:
            self.log_response(e, 'send_email', 'exception')

    def hmac_verification(self, message, hmac_hash):
        hash = hmac.new(self.API_SECRET, message, hashlib.sha256)
        new_hmac_hash = base64.b64encode(hash.digest())
        self.log_response('%s VS %s' % (new_hmac_hash, hmac_hash), 'hmac_verification')
        return new_hmac_hash == hmac_hash

    def log_response(self, response, caller_method='unknown', type='message'):
        if self.Log:
            response = "[ :%s ]: \t%s \n" % (caller_method, response)
            action = {
                'message': self.logger.info(response),
                'exception': self.logger.error(response),
            }

            action[type]


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)
