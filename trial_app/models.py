from django.db import models


class Store(models.Model):
    shopify_store_id = models.IntegerField(null=False, unique=True, default='0')
    name = models.TextField(null=False)
    domain = models.TextField(null=False, default='')

    class Meta:
        db_table = u'trial_app_store'

    def __unicode__(self):
        return self.name,


class Customer(models.Model):
    shopify_customer_id = models.IntegerField(null=False, unique=True, default='0')
    first_name = models.TextField(null=False)
    last_name = models.TextField(null=False)
    email = models.CharField(max_length=80)

    class Meta:
        db_table = u'trial_app_customer'

    def __unicode__(self):
        return self.first_name, self.last_name, self.email


class Orders(models.Model):
    shopify_order_id = models.IntegerField(null=False, unique=True, default='0')
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    name = models.TextField(null=False)
    order_number = models.IntegerField(null=False)
    total_price = models.TextField(null=False)
    currency = models.TextField(null=False, default='')
    financial_status = models.TextField(null=False, default='')
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    email = models.CharField(max_length=80)
    created_at = models.DateTimeField(null=False)
    updated_at = models.DateTimeField(null=False)
    shipping_address = models.TextField(null=False, default='')
    billing_address = models.TextField(null=False, default='')
    notification_sent = models.IntegerField(null=False, default='0')
    incorrect_address = models.IntegerField(null=False, default='0')

    class Meta:
        db_table = u'trial_app_orders'

    def __unicode__(self):
        return self.name, self.amount, self.email
