from django.conf.urls import patterns, include, url
from shopify_app.decorators import shop_login_required
from trial_app import views

urlpatterns = patterns('',
                       url(r'^login/', include('shopify_app.urls')),
                       url(r'^$', shop_login_required(views.Home.as_view()), name='home'),
                       url(r'^webhook/$', views.Webhooks.as_view(), name='webhook'),
                       )
